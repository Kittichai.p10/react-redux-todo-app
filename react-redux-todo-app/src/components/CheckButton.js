import React from 'react';
import { motion, useMotionValue, useTransform } from 'framer-motion';
import styles from '../styles/modules/todoItem.module.scss';

const chackVariants = {
  initial: {
    color: '#fff',
  },
  checked: {
    pathLanfth: 1,
  },
  unchecked: {
    pathLanfth: 0,
  },
};

function CheckButton({ checked, setchecked }) {
  return (
    <motion.div
      className={styles.svgBox}
      animate={checked ? 'checked' : 'unchacked'}
      variants={chackVariants}
      onClick={() => {
        console.log('clicked clicked');
      }}
    >
      <motion.svg
        className={styles.svg}
        viewBox="0 0 53 38"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <motion.path
          //   variants={checkVariants}
          //   animate={checked ? 'checked' : 'unchecked'}
          //   style={{ pathLength, opacity }}
          fill="none"
          strokeMiterlimit="10"
          strokeWidth="6"
          d="M1.5 22L16 36.5L51.5 1"
          strokeLinejoin="round"
          strokeLinecap="round"
        />
      </motion.svg>
    </motion.div>
  );
}

export default CheckButton;
